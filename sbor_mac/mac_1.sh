#/bin/bash
arg4=1
IP=$1
ID=$2
data=$(/bin/date +"%d"."%m"."%y")
snmpwalk -v2c -c metroro $IP 1.3.6.1.2.1.17.7.1.2.2.1.2."$ID" | nawk '{ print substr($1,39) }' | sed -e "s/\./ /g" > tmp/tmp_"$IP".log
snmpwalk -v2c -c metroro $IP 1.3.6.1.2.1.17.7.1.2.2.1.2."$ID" | nawk '{ print $4 }' > tmp/tmp_1_"$IP".log
while read tmp
do
let "arg_4 += 1"
port=$(/bin/cat tmp/tmp_1_"$IP".log | /bin/sed "$arg_4"!d)
MAC=$(/bin/echo $tmp | awk '{printf "%x\n", $1}' | /usr/bin/awk -F: '{for(k=1; k<=NF; k++) {if (length($k)==1) {print('0' $k)} else {print($k)}}}')-$( /bin/echo $tmp | /usr/bin/awk '{printf "%x\n", $2}' | /usr/bin/awk -F: '{for(k=1; k<=NF; k++) {if (length($k)==1) {print('0' $k)} else {print($k)}}}')-$( /bin/echo $tmp | /usr/bin/awk '{printf "%x\n", $3}' | /usr/bin/awk -F: '{for(k=1; k<=NF; k++) {if (length($k)==1) {print('0' $k)} else {print($k)}}}')-$( /bin/echo $tmp | /usr/bin/awk '{printf "%x\n", $4}' | /usr/bin/awk -F: '{for(k=1; k<=NF; k++) {if (length($k)==1) {print('0' $k)} else {print($k)}}}')-$( /bin/echo $tmp | /usr/bin/awk '{printf "%x\n", $5}' | /usr/bin/awk -F: '{for(k=1; k<=NF; k++) {if (length($k)==1) {print('0' $k)} else {print($k)}}}')-$( /bin/echo $tmp | /usr/bin/awk '{printf "%x\n", $6}' | /usr/bin/awk -F: '{for(k=1; k<=NF; k++) {if (length($k)==1) {print('0' $k)} else {print($k)}}}')
MAC_1=$(/bin/echo $MAC | /usr/bin/tr '[a-f]' '[A-F]')
/bin/echo "Порт $port мак $MAC_1" ## >> log/mac_db_"$IP".log
done < tmp/tmp_"$IP".log
/bin/echo "Коммутатор $IP загрузка FDB закончена"
