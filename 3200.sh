#!/usr/bin/expect -f
set IP [lindex $argv 0]
set COMD [lindex $argv 1]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "UserName:"
# Посылаем имя пользователя и ждем запроса пароля.
send "Admin\r"
expect "PassWord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "fhrfynjc\r"
expect "*#"
send "delete vlan vlanid 649\r"
expect "*#"
send "delete vlan vlanid 589\r"
expect "*#"
send "delete vlan vlanid 590\r"
expect "*#"
send "download cfg_fromTFTP 192.168.33.33 DES-3200-28_ALL.cfg increment\r"
expect "*#"
send "config snmp system_name $COMD\r"
expect "*#"
send "config snmp system_location $COMD\r"
expect "*#"
send "save all\r"
expect "*#"
send "logout\r"
expect eof
