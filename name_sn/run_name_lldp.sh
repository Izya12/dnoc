#/bin/bash
cd /home/user/test/name_sn
while read IP
do
SN=$(snmpget -v2c -c metroro $IP iso.3.6.1.4.1.171.12.1.1.12.0 | awk '{print $4}' | sed -e "s/\"//g")
NAME=$(snmpget -v2c -c metroro $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g")
MODEL=$(snmpget -v2c -c metroro $IP iso.3.6.1.2.1.1.1.0 | awk '{print $5}' | sed -e "s/\"//g")
MAC=$(snmpget -v2c -c metroro $IP iso.0.8802.1.1.2.1.3.2.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
BUILD=$(snmpget -v2c -c metroro $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
AGR_NAME=$(snmpwalk -v2c -c metroro $IP 1.0.8802.1.1.2.1.4.1.1.9 | awk '{print $4}')
AGR_PORT=$(snmpwalk -v2c -c metroro $IP 1.0.8802.1.1.2.1.4.1.1.8 | awk '{print $7, $8}' | /bin/sed -e "s/\"//g")
#echo "$MODEL : $NAME - $SN" >> name_sn.db.txt
echo "$IP, $MODEL, $BUILD, $NAME, $SN, $MAC, $AGR_NAME, $AGR_PORT" # >> name_sn.db.txt
done < /home/user/test/name_sn/db_ip.txt
