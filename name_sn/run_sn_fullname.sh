#/bin/bash
. "/etc/CFG_FULL.cfg"

#rm $pwd/db_ip_test.txt
#rm $pwd/db_ip_agr.txt

#nmap --excludefile $pwd/db_ip_exclude.txt -sn 192.168.212.0/22 | egrep -v "Host|nmap" | awk '{print $5}' | grep "[[:digit:]]" > $pwd/db_ip_test.txt
#nmap --excludefile $pwd/db_ip_agr_exclude.txt -sn 192.168.212.2-49 | egrep -v "Host|nmap" | awk '{print $5}' | grep "[[:digit:]]" > $pwd/db_ip_agr.txt

cd $pwd/name_sn
DATE="$(date +"%Y%m%d")"
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#Коммутаторы доступа
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
while read IP
do
HW=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.16.19.3.0 | awk '{ print $4 }' | sed -e "s/\"//g")
if [ "$HW" = "A1" -o "$HW" = "B1" ]
then
#------------------------------------------------------------------------------------------------------------------------------------#
SN=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.4.1.171.12.1.1.12.0 | awk '{print $4}' | sed -e "s/\"//g")
NAMEUL=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g")
MODEL=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.1.1.0 | awk '{print $5}' | sed -e "s/\"//g")
MAC=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.2.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
BUILD=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
PORTOV=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')
VLANIPTV=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.11.113.1.3.2.7.8.1.1| awk '{print $4}' | sed -e "s/\"//g")
VLANPPPoE=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $IP SNMPv2-SMI::mib-2.17.7.1.4.3.1.1 | grep -E "4.3.1.1.[[:digit:]][[:digit:]][[:digit:]][[:digit:]]" | awk -F "." '{print $9}' | awk '{print $1}')
ZANYATO=$(cat $pwd/port_up_down/log/$IP.log | grep "[[:digit:]]" | wc -l)

#echo "$IP, $MODEL, $PORTOV, $ZANYATO, $BUILD, $NAMEUL, $NAMEDOM, $NAMEPOD,$SN, $MAC"
echo "$IP, $MODEL, $PORTOV, $ZANYATO, $BUILD, $NAMEUL, $SN, $MAC, $VLANIPTV, $VLANPPPoE" >> LOG/name_sn.db_fullname_$DATE.csv
#------------------------------------------------------------------------------------------------------------------------------------#
elif [ "$HW" = "C1" ]
then 
#------------------------------------------------------------------------------------------------------------------------------------#
SN=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.4.1.171.12.1.1.12.0 | awk '{print $4}' | sed -e "s/\"//g")
NAMEUL=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g")
MODEL=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.1.1.0 | awk '{print $4}' | sed -e "s/\"//g")
MAC=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.2.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
BUILD=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
PORTOV=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')
VLANIPTV=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.12.64.3.1.1.1 | awk '{print $4}' | sed -e "s/\"//g")
VLANPPPoE=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $IP SNMPv2-SMI::mib-2.17.7.1.4.3.1.1 | grep -E "4.3.1.1.[[:digit:]][[:digit:]][[:digit:]][[:digit:]]" | awk -F "." '{print $9}' | awk '{print $1}')
ZANYATO=$(cat $pwd/port_up_down/log/$IP.log | grep "[[:digit:]]" | wc -l)

#echo "$IP, $MODEL, $PORTOV, $ZANYATO, $BUILD, $NAMEUL, $NAMEDOM, $NAMEPOD,$SN, $MAC"
echo "$IP, $MODEL, $PORTOV, $ZANYATO, $BUILD, $NAMEUL, $SN, $MAC, $VLANIPTV, $VLANPPPoE" >> LOG/name_sn.db_fullname_$DATE.csv
else
#------------------------------------------------------------------------------------------------------------------------------------#
#/bin/echo "$IP HW XZ" 

SN=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.4.1.171.12.1.1.12.0 | awk '{print $4}' | sed -e "s/\"//g")
NAMEUL=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g")
NAMEDOM=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g" | awk -F "-" '{print $2}')
NAMEPOD=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g" | awk -F "-" '{print $3}')
MODEL=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.1.1.0 | awk '{print $5}' | sed -e "s/\"//g")
MAC=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.2.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
BUILD=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
PORTOV=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')
VLANIPTV=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.11.113.1.3.2.7.8.1.1| awk '{print $4}' | sed -e "s/\"//g")
VLANPPPoE=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $IP SNMPv2-SMI::mib-2.17.7.1.4.3.1.1 | grep -E "4.3.1.1.[[:digit:]][[:digit:]][[:digit:]][[:digit:]]" | awk -F "." '{print $9}' | awk '{print $1}')
ZANYATO=$(cat $pwd/port_up_down/log/$IP.log | grep "[[:digit:]]" | wc -l)

#echo "$IP, $MODEL, $PORTOV, $ZANYATO, $BUILD, $NAMEUL, $NAMEDOM, $NAMEPOD,$SN, $MAC"
echo "$IP, $MODEL, $PORTOV, $ZANYATO, $BUILD, $NAMEUL, $SN, $MAC, $VLANIPTV, $VLANPPPoE" >> LOG/name_sn.db_fullname_$DATE.csv

/bin/echo "$IP $HW" >> $pwd/name_sn/erorr_log_full_$DATE.log
#------------------------------------------------------------------------------------------------------------------------------------#
fi
done < $pwd/db_ip_test.txt