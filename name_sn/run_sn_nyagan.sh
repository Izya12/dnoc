#/bin/bash
#nmap -v -sP 192.168.224.0/22 | tail -n +77 | egrep -i -w -v 'host|Raw|Initiating' | awk '{ print $5 }' > /home/user/test/name_sn/db_ip_nyagan.txt
cd /home/user/test/name_sn
rm name_sn.db.csv
VSEGO=0
Community=public
while read IP
do
SN=$(snmpget -v2c -c $Community $IP iso.3.6.1.4.1.171.12.1.1.12.0 | awk '{print $4}' | sed -e "s/\"//g")
NAME=$(snmpget -v2c -c $Community $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g")
MODEL=$(snmpget -v2c -c $Community $IP iso.3.6.1.2.1.1.1.0 | awk '{print $5}' | sed -e "s/\"//g")
MAC=$(snmpget -v2c -c $Community $IP iso.0.8802.1.1.2.1.3.2.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
BUILD=$(snmpget -v2c -c $Community $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
PORTOV=$(snmpwalk -v2c -c $Community $IP iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')
#echo "$MODEL : $NAME - $SN" >> name_sn.db.txt
echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC"
echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC" >> name_sn.db.csv
#done < /home/user/test/authen/erorr_log.log
let "VSEGO += PORTOV"
done < /home/user/test/name_sn/db_ip_nyagan.txt
echo "Всего портов $VSEGO"
