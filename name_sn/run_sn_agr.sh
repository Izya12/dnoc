#/bin/bash
set -o nounset
set -o errexit

. "/etc/CFG_FULL.cfg"

#rm $pwd/db_ip_test.txt
#rm $pwd/db_ip_agr.txt

#nmap --excludefile $pwd/db_ip_exclude.txt -sn 192.168.212.0/22 | egrep -v "Host|nmap" | awk '{print $5}' | grep "[[:digit:]]" > $pwd/db_ip_test.txt
#nmap --excludefile $pwd/db_ip_agr_exclude.txt -sn 192.168.212.2-49 | egrep -v "Host|nmap" | awk '{print $5}' | grep "[[:digit:]]" > $pwd/db_ip_agr.txt

cd $pwd/name_sn
DATE="$(date +"%Y%m%d")"

echo "IP, Модель, Всего портов, FW, Адрес, SN, MAC" > LOG_AGR/name_sn.db_agr_$DATE.csv

while read IP
do
  #------------------------------------------------------------------------------------------------------------------------------------#
  SN=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.4.1.171.12.1.1.12.0 | awk '{print $4}' | sed -e "s/\"//g")
  NAME=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g")
  MODEL=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.1.1.0 | awk '{print $4}' | sed -e "s/\"//g")
  MAC=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.2.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
  BUILD=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
  PORTOV=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')
  echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC" >> LOG_AGR/name_sn.db_agr_$DATE.csv
#------------------------------------------------------------------------------------------------------------------------------------#
done < $pwd/db_ip_agr.txt

