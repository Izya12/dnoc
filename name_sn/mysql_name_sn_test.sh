#!/bin/bash

. "/etc/CFG_FULL.cfg"

USER_MYSQL=root
PASSWORD_MYSQL=121788

DATE="$(date +"%Y%m%d")"

while read DATA_1
do

IP=$(echo $DATA_1 | awk '{print $1}' | sed 's/\,//g')
NAME=$(echo $DATA_1 | awk '{print $6,$7,$8}' | sed 's/\,//g' | sed 's/ /_/g')
MAC=$(echo $DATA_1 | awk '{print $10}')

if [ "$MAC" != "" ]; then

SN=$(echo $DATA_1 | awk '{print $9}' | sed 's/\,//g')
PORT_Z=$(echo $DATA_1 | awk '{print $4}' | sed 's/\,//g')
HW=$(echo $DATA_1 | awk '{print $5}' | sed 's/\,//g')
MODEL_ID_NAME=$(echo $DATA_1 | awk '{print $2}' | sed 's/\,//g')

case "$MODEL_ID_NAME" in

  "DES-3200-28")
    MODEL_ID=1
  ;;
  "DES-3200-28/C1")
    MODEL_ID=2
  ;;
  "DES-3200-52")
    MODEL_ID=3
  ;;
  "DES-3200-26")
    MODEL_ID=4
  ;;
  "DES-3200-10")
    MODEL_ID=5
  ;;
  "DGS-3200-10")
    MODEL_ID=6
  ;;
  "DES-3200-10/C1")
  MODEL_ID=7
  ;;
  "DES-3200-52/C1")
  MODEL_ID=8
  ;;
  "DES-3200-28P/C1")
  MODEL_ID=9
  ;;
esac

id=$(mysql -D switches_db -u root -p121788 -Ne 'SET NAMES UTF8; USE switches_db; SELECT COUNT(`ip_name`.`MAC`) FROM ip_name WHERE `MAC` LIKE "'%$MAC%'"')

if [ "$id" != "0" ]
then

#echo $id
mysql_data=$(mysql -D switches_db -u $USER_MYSQL -p$PASSWORD_MYSQL -Ne 'SET NAMES UTF8; USE switches_db; INSERT INTO `switches_db`.`data_time` (`N`, `PORT_Z`, `HW`, `DATE_TIME`, `MAC`) VALUES (NULL, "'$PORT_Z'", "'$HW'", CURRENT_TIMESTAMP, "'$MAC'")')

else

#echo "Нет мака"
mysql_data=$(mysql -D switches_db -u $USER_MYSQL -p$PASSWORD_MYSQL -Ne 'SET NAMES UTF8; USE switches_db; INSERT INTO `switches_db`.`ip_name` (`N`, `IP`, `NAME`, `MAC`, `SN`, `DATE_TIME`, `ID_MODEL`) VALUES (NULL, "'$IP'", "'$NAME'", "'$MAC'", "'$SN'", CURRENT_TIMESTAMP, "'$MODEL_ID'")')
#echo "$mysql_data"
mysql_data=$(mysql -D switches_db -u $USER_MYSQL -p$PASSWORD_MYSQL -Ne 'SET NAMES UTF8; USE switches_db; INSERT INTO `switches_db`.`data_time` (`N`, `PORT_Z`, `HW`, `DATE_TIME`, `MAC`) VALUES (NULL, "'$PORT_Z'", "'$HW'", CURRENT_TIMESTAMP, "'$MAC'")')
fi

else 
echo "не работает $IP, $NAME, $MAC, $SN"

fi

done < $pwd/name_sn/LOG/name_sn.db_1_$DATE.csv

echo "Finish"
