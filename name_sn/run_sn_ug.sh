#/bin/bash
. "/etc/CFG_FULL.cfg"

cd $pwd/name_sn
DATE="$(date +"%Y%m%d")"
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#Коммутаторы доступа
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
while read IP
do
HW=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.16.19.3.0 | awk '{ print $4 }' | sed -e "s/\"//g")
if [ "$HW" = "A1" ]
then
#------------------------------------------------------------------------------------------------------------------------------------#
SN=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.4.1.171.12.1.1.12.0 | awk '{print $4}' | sed -e "s/\"//g")
NAME=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g")
MODEL=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.1.1.0 | awk '{print $5}' | sed -e "s/\"//g")
MAC=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.2.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
BUILD=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
PORTOV=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')


#echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC"
echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC" >> LOG_NG/name_sn_ng.db_$DATE.csv
#------------------------------------------------------------------------------------------------------------------------------------#
elif [ "$HW" = "C1" ]
then 
#------------------------------------------------------------------------------------------------------------------------------------#
SN=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.4.1.171.12.1.1.12.0 | awk '{print $4}' | sed -e "s/\"//g")
NAME=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g")
MODEL=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.1.1.0 | awk '{print $4}' | sed -e "s/\"//g")
MAC=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.2.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
BUILD=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
PORTOV=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')


#echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC"
echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC" >> LOG_NG/name_sn_ng.db_$DATE.csv
#------------------------------------------------------------------------------------------------------------------------------------#
elif [ "$HW" = "B1" ]
then 
#------------------------------------------------------------------------------------------------------------------------------------#
SN=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.4.1.171.12.1.1.12.0 | awk '{print $4}' | sed -e "s/\"//g")
NAME=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g")
MODEL=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.1.1.0 | awk '{print $5}' | sed -e "s/\"//g")
MAC=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.2.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
BUILD=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
PORTOV=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')


#echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC"
echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC" >> LOG_NG/name_sn_ng.db_$DATE.csv
#------------------------------------------------------------------------------------------------------------------------------------#
else
#------------------------------------------------------------------------------------------------------------------------------------#
#/bin/echo "$IP HW XZ" 

SN=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.4.1.171.12.1.1.12.0 | awk '{print $4}' | sed -e "s/\"//g")
NAME=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g")
MODEL=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.1.1.0 | awk '{print $5}' | sed -e "s/\"//g")
MAC=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.2.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
BUILD=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
PORTOV=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')


#echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC"
echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC" >> LOG_NG/name_sn_ng.db_$DATE.csv

/bin/echo "$IP $HW" >> $pwd/name_sn/erorr_log_$DATE.log
#------------------------------------------------------------------------------------------------------------------------------------#
fi
done < $pwd/db_ip_ug.txt

#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#Коммутаторы агригации
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#

while read IP
do
#------------------------------------------------------------------------------------------------------------------------------------#
SN=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.4.1.171.12.1.1.12.0 | awk '{print $4}' | sed -e "s/\"//g")
NAME=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g")
MODEL=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.1.1.0 | awk '{print $4}' | sed -e "s/\"//g")
MAC=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.2.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
BUILD=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
PORTOV=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')

#echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC"
echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC" >> LOG_AGR_NG/name_sn.db_ng_agr_$DATE.csv
#------------------------------------------------------------------------------------------------------------------------------------#
done < $pwd/db_ip_agr_ng.txt
