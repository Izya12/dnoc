#/bin/bash
cd /home/user/test/name_sn
while read IP
do
SN=$(snmpwalk -v2c -c fhrfynjc $IP iso.3.6.1.2.1.47.1.1.1.1.11.1001 | awk '{print $4}' | sed -e "s/\"//g")
NAME=$(snmpwalk -v2c -c fhrfynjc $IP iso.3.6.1.2.1.1.5.0 | awk '{print $4}' | sed -e "s/\"//g")
MODEL=$(snmpwalk -v2c -c fhrfynjc $IP iso.3.6.1.2.1.47.1.1.1.1.13.1001 | awk '{print $5}' | sed -e "s/\"//g")
MAC=$(snmpwalk -v2c -c fhrfynjc $IP iso.3.6.1.2.1.17.1.1.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
#echo "$MODEL : $NAME - $SN" >> name_sn.db.txt
echo "$IP, $MODEL, $NAME, $SN, $MAC" # >> name_sn.db.txt
done < /home/user/test/name_sn/cisco_ip.txt
