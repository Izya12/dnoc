#!/bin/bash
set -o nounset
set -o errexit

. "/etc/CFG_FULL.cfg"

#rm $pwd/db_ip_test.txt
#rm $pwd/db_ip_agr.txt

#nmap --excludefile $pwd/db_ip_exclude.txt -sn 192.168.212.0/22 | egrep -v "Host|nmap" | awk '{print $5}' | grep "[[:digit:]]" > $pwd/db_ip_test.txt
#nmap --excludefile $pwd/db_ip_agr_exclude.txt -sn 192.168.212.2-49 | egrep -v "Host|nmap" | awk '{print $5}' | grep "[[:digit:]]" > $pwd/db_ip_agr.txt

cd $pwd/name_sn
DATE="$(date +"%Y%m%d")"
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#Коммутаторы доступа
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
# Задаем имена таблици
echo "IP, Модель, HW, FW, Всего портов, Улица, Дом, Подъезд, SN, MAC" > LOG/name_sn.db_$DATE.csv

AllSnmp ()
{
	SN=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.4.1.171.12.1.1.12.0 | awk '{print $4}' | sed -e "s/\"//g")
	NAMEUL=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g" | awk -F "-" '{print $1}')
	NAMEDOM=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g" | awk -F "-" '{print $2}')
	NAMEPOD=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g" | awk -F "-" '{print $3}')
	MAC=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.2.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
	PORTOV=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')
#	VLANPPPoE=$($VARSNMPWALK -v2c -c $CommunityRO $IP 1.3.6.1.2.1.17.7.1.4.3.1.1 | grep -E "4.3.1.1.[[:digit:]][[:digit:]][[:digit:]][[:digit:]]" | awk -F "." '{print $9}' | awk '{print $1}' | head -n 1)
#	VLANPPPoEName=$($VARSNMPWALK -v2c -c $CommunityRO $IP 1.3.6.1.2.1.17.7.1.4.3.1.1.$VLANPPPoE | awk '{print $4}' | sed -e "s/\"//g")
#	ZANYATO=$(echo "1")
return
}

while read IP
do

  HW=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.16.19.3.0 | awk '{ print $4 }' | sed -e "s/\"//g")
  if [ "$HW" = "A1" -o "$HW" = "B1" ]
    then
      MODEL=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.1.1.0 | awk '{print $5}' | sed -e "s/\"//g")
      if [[ "$MODEL" = DES-3200* ]]
      then
	#------------------------------------------------------------------------------------------------------------------------------------#
	AllSnmp
	BUILD=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
	if [ "$PORTOV" = "26" ]
	then
	  VLANIPTV=$($VARSNMPWALK -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.11.113.1.5.2.7.8.1.1 | awk '{print $4}' | sed -e "s/\"//g")
	else
	  VLANIPTV=$($VARSNMPWALK -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.11.113.1.3.2.7.8.1.1 | awk '{print $4}' | sed -e "s/\"//g")
	fi
	#echo "$IP, $MODEL, $HW, $BUILD, $PORTOV, $ZANYATO, $NAMEUL, $NAMEDOM, $NAMEPOD, $SN, $MAC, $VLANIPTV, $VLANPPPoE, $VLANPPPoEName" >> LOG/name_sn.db_$DATE.csv
	echo "$IP, $MODEL, $HW, $BUILD, $PORTOV, $NAMEUL, $NAMEDOM, $NAMEPOD, $SN, $MAC" >> LOG/name_sn.db_$DATE.csv
      elif [[ "$MODEL" = DES-1228* ]]
      then
	AllSnmp
	BUILD=$($VARSNMPGET -v2c -c $CommunityRO $IP  1.3.6.1.2.1.16.19.2.0 | awk '{print $5}' | sed -e "s/\"//g")
	VLANIPTV=$($VARSNMPWALK -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.11.116.2.2.7.8.1.1 | awk '{print $4}')
	#echo "$IP, $MODEL, $HW, $BUILD, $PORTOV, $ZANYATO, $NAMEUL, $NAMEDOM, $NAMEPOD, $SN, $MAC, $VLANIPTV, $VLANPPPoE, $VLANPPPoEName" >> LOG/name_sn.db_$DATE.csv
	echo "$IP, $MODEL, $HW, $BUILD, $PORTOV, $NAMEUL, $NAMEDOM, $NAMEPOD, $SN, $MAC" >> LOG/name_sn.db_$DATE.csv
      fi

    #------------------------------------------------------------------------------------------------------------------------------------#
    elif [ "$HW" = "C1" -o "$HW" = "A3" ]
    then
      #------------------------------------------------------------------------------------------------------------------------------------#
      AllSnmp
      MODEL=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.1.1.0 | awk '{print $4}' | sed -e "s/\"//g")
      BUILD=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
      VLANIPTV=$($VARSNMPWALK -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.12.64.3.1.1.1 | awk '{print $4}' | sed -e "s/\"//g")
      #echo "$IP, $MODEL, $HW, $BUILD, $PORTOV, $ZANYATO, $NAMEUL, $NAMEDOM, $NAMEPOD, $SN, $MAC, $VLANIPTV, $VLANPPPoE, $VLANPPPoEName" >> LOG/name_sn.db_$DATE.csv
      echo "$IP, $MODEL, $HW, $BUILD, $PORTOV, $NAMEUL, $NAMEDOM, $NAMEPOD, $SN, $MAC" >> LOG/name_sn.db_$DATE.csv
      #------------------------------------------------------------------------------------------------------------------------------------#
    else
      #------------------------------------------------------------------------------------------------------------------------------------#
      AllSnmp
      MODEL=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.1.1.0 | awk '{print $5}' | sed -e "s/\"//g")
      BUILD=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
      VLANIPTV=$($VARSNMPWALK -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.11.113.1.3.2.7.8.1.1 | awk '{print $4}' | sed -e "s/\"//g")
      #echo "$IP, $MODEL, $HW, $BUILD, $PORTOV, $ZANYATO, $NAMEUL, $NAMEDOM, $NAMEPOD, $SN, $MAC, $VLANIPTV, $VLANPPPoE, $VLANPPPoEName" >> LOG/name_sn.db_$DATE.csv
      echo "$IP, $MODEL, $HW, $BUILD, $PORTOV, $NAMEUL, $NAMEDOM, $NAMEPOD, $SN, $MAC" >> LOG/name_sn.db_$DATE.csv
      /bin/echo "$IP $HW" >> $pwd/name_sn/erorr_log_$DATE.log
      #------------------------------------------------------------------------------------------------------------------------------------#
  fi


done < $pwd/db_ip_test.txt
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#Коммутаторы агригации
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------------------------------------#
# Задаем имена таблици
#echo "IP, Модель, Всего портов, FW, Адрес, SN, MAC" > LOG_AGR/name_sn.db_agr_$DATE.csv
#
#while read IP
#do
#  #------------------------------------------------------------------------------------------------------------------------------------#
#  SN=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.4.1.171.12.1.1.12.0 | awk '{print $4}' | sed -e "s/\"//g")
#  NAME=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.3.0 | awk '{print $4}' | sed -e "s/\"//g")
#  MODEL=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.1.1.0 | awk '{print $4}' | sed -e "s/\"//g")
#  MAC=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.0.8802.1.1.2.1.3.2.0 | awk '{print $4 , $5 , $6 , $7 , $8 , $9}' | sed -e "s/ /-/g")
#  BUILD=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
#  PORTOV=$($VARSNMPGET -v2c -c $CommunityRO $IP iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')
#  echo "$IP, $MODEL, $PORTOV, $BUILD, $NAME, $SN, $MAC" >> LOG_AGR/name_sn.db_agr_$DATE.csv
#  #------------------------------------------------------------------------------------------------------------------------------------#
#done < $pwd/db_ip_agr.txt
