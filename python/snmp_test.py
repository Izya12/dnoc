#!/usr/bin/env python
#-*- coding: utf8 -*-
import netsnmp

snmp = netsnmp.Session(Version = 2, DestHost='192.168.212.50', Community='metroro')
oids = netsnmp.VarList(netsnmp.Varbind('.1.3.6.1.2.1.17.1.2.0'))
snmp.walk(oids)
for oid in oids:
  oid.print_str()