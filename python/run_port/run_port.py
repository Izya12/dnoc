#!/usr/bin/env python
#-*- coding: utf8 -*-
import MySQLdb


def read_ip(fname):
    """
    В данной функции заданно открытие файла (fname)
    и его построчное считывание
    :param fname:
    :return:
    """
    with open(fname) as fd:
        return set(fd.read().splitlines())


def main():
    """
    Здесь мы конектимся к базе и отправляем sql
    запрос, потом циклом собераем данные и убераем
    дубли и получившееся записываем в фаийл

    :rtype : object
    """
    query = 'SELECT msg FROM logs WHERE host = %s and msg LIKE %s ORDER BY msg ASC'
    db = MySQLdb.connect(host='127.0.0.1', user='root', passwd='121788', db='syslog')
    cursor = db.cursor()
    for ip in read_ip('../db_ip_test.txt'):
        print ip
        cursor.execute(query, (ip, 'Port% link%'))
        result = set(map(lambda a: str(a).split()[1], cursor.fetchall()))
        with open('log/' + ip + '.log', 'w') as fd:
            fd.write('\n'.join(result))


main()