#/bin/bash

. "/etc/CFG_FULL.cfg"

cd $pwd/authen
nmap --excludefile $pwd/db_ip_exclude.txt -sn 192.168.212.0/22 | egrep -v "Host|nmap" | awk '{print $5}' | grep "[[:digit:]]" > $pwd/db_ip_test.txt
rm erorr_log.log
while read ip
do
tacacs=$(snmpwalk -v2c -c $CommunityRO $ip 1.3.6.1.4.1.171.12.5.10.1.1| awk '{print $4}')
if [ "$tacacs" = "10.18.0.3" ]
then
#echo "$tacacs"
echo "Коммутатор $ip заведен в tacacs+"
else
echo "нужно завести коммутатор $ip"
#echo "$tacacs"
echo "$ip" >> erorr_log.log
fi
done < $pwd/db_ip_test.txt
