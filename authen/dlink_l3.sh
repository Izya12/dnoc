#!/usr/bin/expect -f
set IP [lindex $argv 0]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "*ser*ame:"
# Посылаем имя пользователя и ждем запроса пароля.
send "Admin\r"
expect "*ass*ord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "fhrfynjc\r"
expect "*#"
send "create authen server_host 10.18.0.3 protocol tacacs+ port 49 key \"PVZw2C300048)\" timeout 10 retransmit 5\r"
expect "*#"
send "config authen_login default method local\r"
expect "*#"
send "config authen_enable default method  tacacs+\r"
expect "*#"
send "create authen_login method_list_name TACACS+_login\r"
expect "*#"
send "config authen_login method_list_name TACACS+_login method tacacs+ local\r"
expect "*#"
send "create authen_enable method_list_name TACACS+_enable\r"
expect "*#"
send "config authen_enable method_list_name TACACS+_enable method tacacs+\r"
expect "*#"
send "config authen application all login method_list_name TACACS+_login\r"
expect "*#"
send "config authen application console login default\r"
expect "*#"
send "config authen application all enable method_list_name TACACS+_enable\r"
expect "*#"
send "config authen_enable default method tacacs+\r"
expect "*#"
send "enable authen_policy\r"
expect "*#"
send "save all\r"
expect "*#"
send "logout\r"
expect eof
