#/bin/bash
IP=$1
NAME1=$2
cd /home/user/test/topolg
snmpwalk -v2c -c metroro $IP 1.0.8802.1.1.2.1.4 | grep iso.0.8802.1.1.2.1.4.1.1.9. | awk '{print $4}' > /home/user/test/topolg/topol_tmp.log
echo "subgraph $NAME1 {" >> topokogi.dot
while read top
do
echo ""$NAME1"->$top;" >> topokogi.dot
done < /home/user/test/topolg/topol_tmp.log
echo "}" >> topokogi.dot
