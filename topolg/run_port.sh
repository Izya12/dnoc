#/bin/bash
cd /home/user/test/port_up_down
while read ip
do
cat /var/log/remote/switches.log | grep $ip | grep Port | awk '{ print $7 }' | sort -n | uniq >> /home/user/test/port_up_down/tmp/tmp_$ip.log
cat /home/user/test/port_up_down/tmp/tmp_$ip.log | sort -n | uniq > /home/user/test/port_up_down/log/$ip.log
done < /home/user/test/port_up_down/db_3200.txt
