#!/usr/bin/expect -f
log_file -a /var/log/vpn.out
set IP [lindex $argv 0]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
sleep 3
spawn telnet 192.168.216.1
match_max 100000  
expect "Username:"
# Посылаем имя пользователя и ждем запроса пароля.
send "routing\r"    
expect "Password:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "ghbvjnfltygbkbec\r"
expect "*#"
send "sh arp | in $IP\r"
for  {} 1 {} { 
       expect { 
             -timeout 3
             "*More*" { send " "
                           set results $expect_out(buffer)
                           set file [open "./tmp/$IP.txt" a+];
                           puts $file $results
                           close $file }
             "*#" { set results $expect_out(buffer)
                       set file [open "./tmp/$IP.txt" a+];
                       puts $file $results
                       close $file
                       exit }        
              }
           }
expect "*#"    
send "exit\r"
expect eof
