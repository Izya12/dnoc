#!/usr/bin/expect -f
set IP [lindex $argv 0]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "UserName:"
# Посылаем имя пользователя и ждем запроса пароля.
send "Admin\r"
expect "PassWord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "fhrfynjc\r"
expect "*#"
send "delete snmp community public\r"
expect "*#"
send "delete snmp community private\r"
expect "*#"
send "create snmp community metroro view CommunityView read_only\r"
expect "*#"
send "create snmp community metrorw view CommunityView read_write\r"
expect "*#"
send "enable snmp traps\r"
expect "*#"
send "enable snmp authenticate_traps\r"
expect "*#"
send "enable snmp linkchange_traps\r"
expect "*#"
send "config snmp linkchange_traps ports all enable\r"
expect "*#"
send "logout\r"
expect eof
