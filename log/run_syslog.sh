#/bin/bash
. "/etc/CFG_FULL.cfg"

cd $pwd/log

while read IP
do
status=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.12.12.1.0 | awk '{print $4}')
if [ "$status" = "3" ]
then
echo "Syslog enable to $IP"
else
status_enable=$(/usr/local/bin/snmpset -v2c -c $CommunityRW $IP 1.3.6.1.4.1.171.12.12.1.0 i 3 | awk '{print $4}')
if [ "$status_enable" = "3" ]
then
echo "Syslog desable to enable $IP"
else
echo "Error $IP"
fi
fi

done < $pwd/db_ip_test.txt