#!/usr/bin/expect -f
set IP [lindex $argv 0]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "?ser?ame:"
# Посылаем имя пользователя и ждем запроса пароля.
send "robot\r"
expect "*ass*ord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "7kArzIITio\r"
expect "*#"
send "enable loopdetect\r"
expect "*#"
send "config loopdetect recover_timer 60\r"
expect "*#"
send "config loopdetect interval 10\r"
expect "*#"
send "config loopdetect mode port-based\r"
expect "*#"
send "config loopdetect trap both\r"
expect "*#"
send "config loopdetect ports 1-24 state enabled\r"
expect "*#"
send "config loopdetect ports 25-28 state disabled\r"
expect "*#"
send "logout\r"
expect eof
