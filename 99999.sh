#!/usr/bin/expect -f
log_file -a /var/log/vpn.out
set IP [lindex $argv 0]
set IP2 [lindex $argv 1]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
sleep 3
spawn telnet 192.168.216.1
match_max 100000  
expect "Username:"
send "routing\r"    
expect "Password:"
send "ghbvjnfltygbkbec\r"
expect "*#"
send "sh arp | in $IP\r"
for  {} 1 {} { 
       expect { 
             -timeout 3
             "*More*" { send " "
                           set results $expect_out(buffer)
                           set file [open "./tmp/$IP.txt" a+];
                           puts $file $results
                           close $file }
             "*#" { set results $expect_out(buffer)
                       set file [open "./tmp/IP.txt" a+];
                       puts $file $results
                       close $file
                       exit }        
              }
           }
expect "*#"    
send "exit\r"
sleep 2
spawn telnet $IP2
match_max 100000  
expect "UserName:"
# Посылаем имя пользователя и ждем запроса пароля.
send "Admin\r"
expect "PassWord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "fhrfynjc\r"
expect "*#"
set MAC=`cat tmp/$IP.txt | egrep -n '[[:alnum:]]{2}.[[:alnum:]]{2}.[[:alnum:]]{2}.[[:alnum:]]{2}' | cut -d' ' -f14 `
send "show fdb mac_address $MAC\r"
expect "*#"    
send "logout\r"
expect eof
