#/bin/bash
. "/etc/CFG_FULL.cfg"
cd $pwd/Security

#nmap --excludefile $pwd/db_ip_exclude.txt -sn 192.168.212.0/22 | egrep -v "Host" | awk '{print $5}' > $pwd/db_ip_test.txt
#nmap --excludefile $pwd/db_ip_agr_exclude.txt -sn 192.168.212.2-49 | egrep -v "Host" | awk '{print $5}' > $pwd/db_ip_agr.txt

# Коммутаторы доступа
while read ip
do

HW=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $ip iso.3.6.1.2.1.16.19.3.0 | awk '{ print $4 }' | sed -e "s/\"//g")

################################################################################################################################

if [ "$HW" = "C1" ]
then

PORT=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $ip iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')

if [ "$PORT" = "10" ]
then

#COMD1="config stp ports 1-8 fbpdu disable"
#COMD2="config loopdetect ports 1-8 state enabled"
COMD3="config traffic control 1-8 broadcast enable multicast enable unicast disable action shutdown threshold 300 countdown 5 time_interval 5"
#COMD4="config filter dhcp_server ports 1-8 state enable"

#$pwd/Security/comd.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD4"
$pwd/Security/comd.sh "$ip" "$COMD3"

elif [ "$PORT" = "28" ]
then 

#COMD1="config stp ports 1-24 fbpdu disable"
#COMD2="config loopdetect ports 1-24 state enabled"
COMD3="config traffic control 1-24 broadcast enable multicast enable unicast disable action shutdown threshold 300 countdown 5 time_interval 5"
#COMD4="config filter dhcp_server ports 1-24 state enable"

#$pwd/Security/comd.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD4"
$pwd/Security/comd.sh "$ip" "$COMD3"

elif [ "$PORT" = "52" ]
then 

#COMD1="config stp ports 1-48 fbpdu disable"
#COMD2="config loopdetect ports 1-48 state enabled"
COMD3="config traffic control 1-48 broadcast enable multicast enable unicast disable action shutdown threshold 300 countdown 5 time_interval 5"
#COMD4="config filter dhcp_server ports 1-48 state enable"

#$pwd/Security/comd.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD4"
$pwd/Security/comd.sh "$ip" "$COMD3"

else
echo "$ip PORT XZ" 
fi

#elif [ "$HW" = "A1" ]
#then
#
#PORT=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $ip iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')
#
#if [ "$PORT" = "10" ]
#then
#
#COMD1="config filter dhcp_server ports 1-8 state enabled"
#COMD2="config filter dhcp_server trap enable"
#COMD3="config filter dhcp_server log enable"
#COMD4="config traffic control 1-8 broadcast enable multicast enable unicast disable action shutdown threshold 1024 countdown 5 time_interval 5"
#
#$pwd/Security/comd_A1_B1.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD3"
#$pwd/Security/comd_A1_B1.sh "$ip" "$COMD4"
#
#elif [ "$PORT" = "28" ]
#then 
#
#COMD1="config filter dhcp_server ports 1-24 state enable"
#COMD2="config filter dhcp_server trap enable"
#COMD3="config filter dhcp_server log enable"
#COMD4="config traffic control 1-24 broadcast enable multicast enable unicast disable action shutdown threshold 1024 countdown 5 time_interval 5"
#
#$pwd/Security/comd_A1_B1.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD3"
#$pwd/Security/comd_A1_B1.sh "$ip" "$COMD4"

#elif [ "$PORT" = "52" ]
#then 

#COMD1="config filter dhcp_server ports 1-48 state enabled"
#COMD2="config filter dhcp_server trap enable"
#COMD3="config filter dhcp_server log enable"
#COMD4="config traffic control 1-48 broadcast enable multicast enable unicast disable action shutdown threshold 1024 countdown 5 time_interval 5"

#$pwd/Security/comd_A1_B1.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD3"
#$pwd/Security/comd_A1_B1.sh "$ip" "$COMD4"

#else
#echo "$ip PORT XZ" 
#fi

#elif [ "$HW" = "B1" ]
#then 

#PORT=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $ip iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')

#if [ "$PORT" = "10" ]
#then

#COMD1="config filter dhcp_server ports 1-8 state enabled"
#COMD2="config filter dhcp_server trap enable"
#COMD3="config filter dhcp_server log enable"
#COMD4="config traffic control 1-8 broadcast enable multicast enable unicast disable action shutdown threshold 1024 countdown 5 time_interval 5"

#$pwd/Security/comd_A1_B1.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD3"
#$pwd/Security/comd_A1_B1.sh "$ip" "$COMD4"

#elif [ "$PORT" = "28" ]
#then 

#COMD1="config filter dhcp_server ports 1-24 state enable"
#COMD2="config filter dhcp_server trap enable"
#COMD3="config filter dhcp_server log enable"
#COMD4="config traffic control 1-24 broadcast enable multicast enable unicast disable action shutdown threshold 1024 countdown 5 time_interval 5"

#$pwd/Security/comd_A1_B1.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD3"
#$pwd/Security/comd_A1_B1.sh "$ip" "$COMD4"

#elif [ "$PORT" = "52" ]
#then 

#COMD1="config filter dhcp_server ports 1-48 state enabled"
#COMD2="config filter dhcp_server trap enable"
#COMD3="config filter dhcp_server log enable"
#COMD4="config traffic control 1-48 broadcast enable multicast enable unicast disable action shutdown threshold 1024 countdown 5 time_interval 5"

#$pwd/Security/comd_A1_B1.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD3"
#$pwd/Security/comd_A1_B1.sh "$ip" "$COMD4"

#else
#echo "$ip PORT XZ" 
#fi

else

echo "$ip to $HW"

fi

################################################################################################################################

done < $pwd/db_ip_test.txt
