#!/usr/bin/expect -f
set IP [lindex $argv 0]
set COMD1 [lindex $argv 1]
set COMD2 [lindex $argv 2]
#set COMD3 [lindex $argv 3]
#set COMD4 [lindex $argv 4]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "*ser*ame:"
# Посылаем имя пользователя и ждем запроса пароля.
send "robot\r"
expect "*ass*ord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "7kArzIITio\r"
set timeout -1
expect "*#"
#send "config loopdetect recover_timer 0\r"
#expect "*#"
#send "config loopdetect interval 1\r"
#expect "*#"
#send "enable loopdetect\r"
#expect "*#"
#send "config filter dhcp_server illegal_server_log_suppress_duration 30min\r"
#expect "*#"
send "config safeguard_engine state enable utilization rising 90 falling 70 trap_log enable mode fuzzy\r"
expect "*#"
send "$COMD1\r"
expect "*#"
send "$COMD2\r"
#expect "*#"
#send "$COMD3\r"
#expect "*#"
#send "$COMD4\r"
expect "*#"
send "save all\r"
expect "*#"
send "logout\r"
expect eof
