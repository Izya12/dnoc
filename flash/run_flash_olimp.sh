#/bin/bash
cd /home/user/test/flash
#while read ip
#do
ip=$1
/home/user/test/flash/dlink_olimp.sh $ip
echo "Reboot $ip"
snmpset -v2c -c metrorw $ip 1.3.6.1.4.1.171.12.1.2.19.0 i 2
/bin/sleep 2m
BUILD=$(snmpget -v2c -c metroro $ip iso.3.6.1.2.1.47.1.1.1.1.9.1 | awk '{print $4}' | sed -e "s/\"//g")
count=$(ping -c 4 $ip | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
if [ $count -eq 0 ];
then
echo "Хост $ip не пингуется $(date)"
else
echo "Хост $ip пингуется HW: $BUILD"
fi

#done < /home/user/test/flash/db_ip_1.txt
