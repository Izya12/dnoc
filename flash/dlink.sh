#!/usr/bin/expect -f
set IP [lindex $argv 0]
set COMD_1 [lindex $argv 1]
set COMD_2 [lindex $argv 2]
set COMD_3 [lindex $argv 3]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "*ser*ame:"
# Посылаем имя пользователя и ждем запроса пароля.
send "$COMD_1\r"
expect "*ass*ord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "$COMD_2\r"
set timeout -1
expect "*#"
send "$COMD_3\r"
expect "*#"
#send "save all\r"
#expect "*#"
send "logout\r"
expect eof
