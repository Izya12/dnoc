#/bin/bash
cd /home/user/test/cable_diag
IP=$1
PORT=$2
PORT_2=$3
echo "С порта $PORT"
echo "По порт $PORT_2"
##запускаем тест кабеля
for i in `seq $PORT $PORT_2`; do snmpset -v 2c -c metrorw $IP 1.3.6.1.4.1.171.12.58.1.1.1.12.$i i 1; done > /dev/null
##проверяем статус
for i in `seq $PORT $PORT_2`; do snmpwalk -v 2c -c metrorw $IP 1.3.6.1.4.1.171.12.58.1.1.1.12.$i; done
## Смотрим длину 1 пары
for i in `seq $PORT $PORT_2`; do snmpwalk -v 2c -c metrorw $IP 1.3.6.1.4.1.171.12.58.1.1.1.8.$i; done
## Смотрим длину 2 пары
for i in `seq $PORT $PORT_2`; do snmpwalk -v 2c -c metrorw $IP 1.3.6.1.4.1.171.12.58.1.1.1.9.$i; done
## Смотрим длину 3 пары
for i in `seq $PORT $PORT_2`; do snmpwalk -v 2c -c metrorw $IP 1.3.6.1.4.1.171.12.58.1.1.1.10.$i; done
## Смотрим длину 4 пары
for i in `seq $PORT $PORT_2`; do snmpwalk -v 2c -c metrorw $IP 1.3.6.1.4.1.171.12.58.1.1.1.11.$i; done
##
