#!/bin/bash
USER=$1
cat /dev/null > /home/user/test/test/temp_log
cat /dev/null > /home/user/test/test/temp_mac_DLINK
cat /dev/null > /home/user/test/test/vlan
cat /dev/null > /home/user/test/test/des-3200.tmp
rsh -l routing 217.115.176.65 sh us | grep $USER | awk '{ print $1 }' > /home/user/test/test/temp_log
ID=$(tail -n 1 /home/user/test/test/temp_log)
if [[ $ID = "" ]]
then
echo "Абонент не в сети"
else
rsh -l routing 217.115.176.65 sh pppoe sess | grep $ID | awk '{ print $3 }' > /home/user/test/test/temp_mac
tmp=$(tail -n 1 /home/user/test/test/temp_mac | sed 's/a/A/g;s/b/B/g;s/c/C/g;s/d/D/g;s/e/E/g;s/f/F/g;s/\.\|:\|-//g')
MAC="$(echo $tmp | cut -c 1,2)-$(echo $tmp | cut -c 3,4)-$(echo $tmp | cut -c 5,6)-$(echo $tmp | cut -c 7,8)-$(echo $tmp | cut -c 9,10)-$(echo $tmp | cut -c 11,12)"
echo $MAC > /home/user/test/test/temp_mac_DLINK
rsh -l routing 217.115.176.65 sh pppoe sess | grep $ID | awk '{ print $4 }' > /home/user/test/test/vlan
VLAN=$(tail -n 1 /home/user/test/test/vlan)
cat /home/user/test/test/DB.txt | grep $VLAN | awk '{ print $1 }' > /home/user/test/test/des-3200.tmp
fi


