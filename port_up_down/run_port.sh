#/bin/bash
. "/etc/CFG_FULL.cfg"

rm $pwd/port_up_down/no_syslog.log

cd $pwd/port_up_down
while read ip
do
	cat /dev/null > $pwd/port_up_down/tmp/tmp_$ip.log
	grep -w $ip /var/log/remote/switches.log | grep Port | awk '{ print $7 }' | sort -n | uniq >> $pwd/port_up_down/tmp/tmp_$ip.log
	grep -w $ip /var/log/remote/switches.log.1 | grep Port | awk '{ print $7 }' | sort -n | uniq >> $pwd/port_up_down/tmp/tmp_$ip.log
	zcat /var/log/remote/switches.log.2.gz | grep " $ip " | grep Port | awk '{ print $7 }' | sort -n | uniq >> $pwd/port_up_down/tmp/tmp_$ip.log
	cat $pwd/port_up_down/tmp/tmp_$ip.log | sort -n | uniq | grep "[[:digit:]]" > $pwd/port_up_down/log/$ip.log

	LINES=$(/usr/bin/wc -l $pwd/port_up_down/log/$ip.log | awk '{print $1}')

	if [ "$LINES" -lt "2" ]
	then
		echo "$ip no syslog"
		echo "$ip" >> $pwd/port_up_down/no_syslog.log
	fi

done < $pwd/db_ip_test.txt
