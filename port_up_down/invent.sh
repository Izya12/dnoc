#/bin/bash
. "/etc/CFG_FULL.cfg"

cd $pwd/port_up_down
rm $pwd/port_up_down/invent.log
while read ip
do
LINES=$(/usr/bin/wc -l $pwd/port_up_down/log/$ip.log | awk '{print $1}')
PORTOV=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $ip iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')
let PORTOV2=PORTOV-6

if [ "$LINES" -ge "$PORTOV2" ]
then
echo "$ip Емкость коммутатора $PORTOV занято $LINES"
echo "$ip" >> $pwd/port_up_down/invent.log
fi

done < $pwd/db_ip_test.txt