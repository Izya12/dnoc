#/bin/bash
. "/etc/CFG_FULL.cfg"
cd $pwd/lldp

# Коммутаторы доступа
while read IP
do

PORT=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $IP iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')

if [ "$PORT" = "10" ]
then

for id_port in {1..10}
do
status=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.11.113.1.3.2.2.1.1.4.$id_port.100 | awk '{ print $4 }' | tail -n 1)
echo "$IP порт $id_port $status" > $pwd/port_up_down/tmp/"$IP"_temp1.log
cat $pwd/port_up_down/tmp/"$IP"_temp1.log | grep -v '3' | awk '{ print $3 }' | sort -n >> $pwd/port_up_down/tmp/tmp_$IP.log
done

elif [ "$PORT" = "28" ]
then 

for id_port in {1..28}
do
status=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.11.113.1.3.2.2.1.1.4.$id_port.100 | awk '{ print $4 }' | tail -n 1)
echo "$IP порт $id_port $status" > $pwd/port_up_down/tmp/"$IP"_temp1.log
cat $pwd/port_up_down/tmp/"$IP"_temp1.log | grep -v '3' | awk '{ print $3 }' | sort -n >> $pwd/port_up_down/tmp/tmp_$IP.log
done

elif [ "$PORT" = "52" ]
then 

for id_port in {1..52}
do
status=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.11.113.1.3.2.2.1.1.4.$id_port.100 | awk '{ print $4 }' | tail -n 1)
echo "$IP порт $id_port $status" > $pwd/port_up_down/tmp/"$IP"_temp1.log
cat $pwd/port_up_down/tmp/"$IP"_temp1.log | grep -v '3' | awk '{ print $3 }' | sort -n >> $pwd/port_up_down/tmp/tmp_$IP.log
done

else
echo "$ip PORT XZ" 
echo "$ip" >> erorr_log.log
fi

done < $pwd/db_ip_test.txt
