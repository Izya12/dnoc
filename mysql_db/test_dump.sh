#!/bin/bash
DIR="/home/user/test/mysql_db"
mkdir -p $DIR
LOG="/home/user/test/mysql_db/log.log"
touch $LOG
TIMENAME=`date +%d.%m.%Y-%H.%M`
db=`mysql -u root -h localhost -p121788 -Bse 'show databases'`
for n in $db; do
TIMEDUMP=`date '+%T %x'`
echo "backup has been done at $TIMEDUMP : $TIMENAME on db: $n" >> $LOG
mysqldump -u root -h localhost -p121788 $n | gzip -c > "$DIR/mysql-$n-$TIMENAME-db.dump.gz"
done
