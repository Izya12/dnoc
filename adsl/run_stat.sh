#/bin/bash
cd /home/user/test/adsl
rm /home/user/test/adsl/db_adsl_32.4.csv
IP=$1
echo "port downstream_margin upstream_margin attainable_downstream_rate attainable_upstream_rate downstream upstream" > /home/user/test/adsl/db_adsl_32.4.csv
while read port_num
do

SnrMgn_down=$(snmpwalk -v2c -c fhrfynjc $IP .1.3.6.1.2.1.10.94.1.1.2.1.4.$port_num | awk '{print $4}')
SnrMgn_up=$(snmpwalk -v2c -c fhrfynjc $IP .1.3.6.1.2.1.10.94.1.1.3.1.4.$port_num | awk '{print $4}')
Atn_down=$(snmpwalk -v2c -c fhrfynjc $IP .1.3.6.1.2.1.10.94.1.1.2.1.5.$port_num | awk '{print $4}')
Atn_up=$(snmpwalk -v2c -c fhrfynjc $IP .1.3.6.1.2.1.10.94.1.1.3.1.5.$port_num | awk '{print $4}')
upstream=$(snmpwalk -v2c -c fhrfynjc $IP .1.3.6.1.2.1.10.94.1.1.5.1.2.$port_num | awk '{print $4}')
downstream=$(snmpwalk -v2c -c fhrfynjc $IP .1.3.6.1.2.1.2.2.1.5.$port_num | awk '{print $4}')

echo "$port_num $SnrMgn_up $SnrMgn_down $Atn_up $Atn_down $downstream $upstream" >> /home/user/test/adsl/db_adsl_32.4.csv

done < /home/user/test/adsl/port.list


echo "********************"
echo
cat /home/user/test/adsl/db_adsl_32.4.csv
echo
echo "********************"

#/home/user/test/des/dlink_des.sh $IP