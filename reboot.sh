#/bin/bash
cd /home/user/test/flash
while read ip
do
echo "Reboot $ip"
/home/user/test/flash/snmp.sh $ip
snmpset -v2c -c metrorw $ip 1.3.6.1.4.1.171.12.1.2.3.0 i 3 > /dev/null
/bin/sleep 60
count=$(ping -c 4 $ip | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
if [ $count -eq 0 ];
then
echo "Хост $ip не пингуется $(date)"
else
echo "Хост $ip пингуется"
fi
done < /home/user/test/flash/db_ip_1.txt

