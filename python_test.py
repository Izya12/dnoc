#!/usr/bin/env python
# -*- coding: utf-8 -*-
#A System Information Gathering Script
import subprocess

def uname_func():
    uname = "uname"
    uname_arg = "-a"
    print "Собираем системную информацию с помощью команды %s:\n" % uname
    subprocess.call([uname, uname_arg])    
    print "\n"

def disk_func():
    diskspace = "df"
    diskspace_arg = "-h"
    print "Собираем информацию о свободном месте на дисках с помощью команды %s:\n" % diskspace
    subprocess.call([diskspace, diskspace_arg])
    print "\n"
    
def tmp_space():
    tmp_usage = "du"
    tmp_arg = "-h"
    path = "/tmp"
    print "Свободное место в директории /tmp:\n"
    subprocess.call([tmp_usage, tmp_arg, path])
    print "\n"

#Main function that call other functions
def main():
    uname_func()
    disk_func()
    tmp_space()

if __name__ == "__main__":
    main()  
