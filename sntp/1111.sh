#!/usr/bin/expect -f
set IP [lindex $argv 0]
set COMD [lindex $argv 1]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "*ser*ame:"
# Посылаем имя пользователя и ждем запроса пароля.
send "robot\r"
expect "*ass*ord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "7kArzIITio\r"
expect "*#"
send "disable sntp\r"
expect "*#"
send "enable sntp\r"
expect "*#"
send "config sntp primary 192.168.33.33 secondary 192.168.212.1 poll-interval 720\r"
expect "*#"
send "config dst disable\r"
expect "*#"
send "$COMD\r"
expect "*#"
send "logout\r"
expect eof
