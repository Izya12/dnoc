#/bin/bash

. "/etc/CFG_FULL.cfg"

cd $pwd/cfg
data=$(date '+%d.%m.%y')
while read ip
do
#MODEl=$(snmpwalk -v2c -c $CommunityRO $ip SNMPv2-MIB::sysDescr.0 | awk '{ print $4 }')
#if [ "$MODEl" = "DGS-3627G" ]
#then

name=bacup_cfg/agr_"$ip"/"$data"_"$ip".cfg

if [ -d /srv/tftp/bacup_cfg/agr_$ip/ ]
then
touch /srv/tftp/$name
chmod 777 /srv/tftp/$name
$VARSNMPSET -v2c -c $CommunityRW $ip 1.3.6.1.4.1.171.12.1.2.18.1.1.3.3 a 192.168.33.33 \
1.3.6.1.4.1.171.12.1.2.18.1.1.5.3 s $name \
1.3.6.1.4.1.171.12.1.2.18.1.1.6.3 i 4 \
1.3.6.1.4.1.171.12.1.2.18.1.1.7.3 s STARTUP.CFG \
1.3.6.1.4.1.171.12.1.2.18.1.1.8.3 i 2 \
1.3.6.1.4.1.171.12.1.2.18.1.1.12.3 i 3
$VARSNMPSET -v2c -c $CommunityRW $ip 1.3.6.1.4.1.171.12.1.2.18.4.0 i 2


else

/bin/echo "Create bacup_cfg/agr_$ip/"
mkdir /srv/tftp/bacup_cfg/agr_$ip/
touch /srv/tftp/$name
chmod 777 /srv/tftp/$name
$VARSNMPSET -v2c -c $CommunityRW $ip 1.3.6.1.4.1.171.12.1.2.18.1.1.3.3 a 192.168.33.33 \
1.3.6.1.4.1.171.12.1.2.18.1.1.5.3 s $name \
1.3.6.1.4.1.171.12.1.2.18.1.1.6.3 i 4 \
1.3.6.1.4.1.171.12.1.2.18.1.1.7.3 s STARTUP.CFG \
1.3.6.1.4.1.171.12.1.2.18.1.1.8.3 i 2 \
1.3.6.1.4.1.171.12.1.2.18.1.1.12.3 i 3
$VARSNMPSET -v2c -c $CommunityRW $ip 1.3.6.1.4.1.171.12.1.2.18.4.0 i 2

fi

#elif [ "$MODEl" = "DGS-6604" ]
#echo "DGS-6604"
#then

#else
#echo "$ip $MODEl" 
#echo "$ip $MODEl" >> erorr_log_agr_$data.log
#fi
done < $pwd/db_ip_agr.txt