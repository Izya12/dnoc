#/bin/bash
. "/etc/CFG_FULL.cfg"
cd $pwd/cfg

filname=$(date +%Y.%m.%d_%H:%M)

touch $pwd/cfg/tmp/$filname.com
nano $pwd/cfg/tmp/$filname.com
nano $pwd/cfg/tmp/db_ip_agr.txt

scount=$(wc -l $pwd/cfg/tmp/$filname.com)

if [ "$scount" != "0" ]
then

cat $pwd/cfg/template/head.tpl > $pwd/cfg/tmp/$filname.exp

while read line
do
sed -e "s/%%%%%/$line/g" $pwd/cfg/template/body.tpl >> $pwd/cfg/tmp/$filname.exp
done < $pwd/cfg/tmp/$filname.com

cat $pwd/cfg/template/tail.tpl >> $pwd/cfg/tmp/$filname.exp
chmod +x $pwd/cfg/tmp/$filname.exp

# Коммутаторы агрегации

while read ip
do

$pwd/cfg/tmp/$filname.exp $ip >> $pwd/cfg/$filname.log

echo "------------------------------------------------------------------------------------------------$ip---------------------------------------------------------------------------------------"

done < $pwd/cfg/tmp/db_ip_agr.txt

else
echo "$ip PORT XZ"
fi

rm $pwd/cfg/tmp/$filname.com
rm $pwd/cfg/tmp/db_ip_agr.txt
rm $pwd/cfg/tmp/$filname.exp