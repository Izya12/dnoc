#/bin/bash

. "/etc/CFG_FULL.cfg"

cd $pwd/cfg
data=$(date '+%d.%m.%y')
while read ip
do
HW=$(snmpwalk -v2c -c $CommunityRO $ip iso.3.6.1.2.1.16.19.3.0 | awk '{ print $4 }' | sed -e "s/\"//g")
if [ "$HW" = "A1" ]
then
COMD="upload cfg_toTFTP 192.168.33.33 bacup_cfg/A1_"$ip"/"$data"_"$ip".cfg"
$pwd/cfg/1111.sh $ip "$COMD"
#mv /srv/tftp/bacup_cfg/*_$ip.cfg /srv/tftp/bacup_cfg/A1_$ip/
elif [ "$HW" = "C1" ]
then 
COMD="upload cfg_toTFTP 192.168.33.33 dest_file bacup_cfg/C1_"$ip"/"$data"_"$ip".cfg"
$pwd/cfg/1111.sh $ip "$COMD"
#mv /srv/tftp/bacup_cfg/*_$ip.cfg /srv/tftp/bacup_cfg/C1_$ip/
elif [ "$HW" = "B1" ]
then 
COMD="upload cfg_toTFTP 192.168.33.33 bacup_cfg/B1_"$ip"/"$data"_"$ip".cfg"
$pwd/cfg/1111.sh $ip "$COMD"
#mv /srv/tftp/bacup_cfg/*_$ip.cfg /srv/tftp/bacup_cfg/B1_$ip/
else
echo "$ip HW XZ" 
echo "$ip" >> erorr_log_$data.log
fi
done < $pwd/db_ip_test.txt
