#/bin/bash

. "/etc/CFG_FULL.cfg"

SNMPSET=` which snmpset`
TFTP_SERVER_IP='10.109.16.30'

DATE=`date '+%d.%m.%y'`


cd $pwd/cfg
while read ip
do
  HW=$(snmpwalk -v2c -c $CommunityRO $ip iso.3.6.1.2.1.16.19.3.0 | awk '{ print $4 }' | sed -e "s/\"//g")
  if [ "$HW" = "A1" -o "$HW" = "B1" ]
  then

    FILE_NAME=bacup_cfg/"$ip"/"$DATE"_"$ip".cfg
    if [ -d /srv/tftp/$ip/ ]
    then
    touch /srv/tftp/$FILE_NAME
    chmod 777 /srv/tftp/$FILE_NAME
    else
    mkdir /srv/tftp/bacup_cfg/$ip/ > /dev/null
    touch /srv/tftp/$FILE_NAME
    chmod 777 /srv/tftp/$FILE_NAME
    fi
    $SNMPSET -v2c -c $CommunityRW $ip 1.3.6.1.4.1.171.12.1.2.6.0 i 2 > /dev/null
    $SNMPSET -v2c -c $CommunityRW $ip 1.3.6.1.4.1.171.12.1.2.1.1.3.3 a $TFTP_SERVER_IP \
    1.3.6.1.4.1.171.12.1.2.1.1.4.3 i 2 \
    1.3.6.1.4.1.171.12.1.2.1.1.5.3 s $FILE_NAME \
    1.3.6.1.4.1.171.12.1.2.1.1.7.3 i 2 \
    1.3.6.1.4.1.171.12.1.2.1.1.8.3 i 3 > /dev/null
    sleep 3
  elif [ "$HW" = "C1" -o "$HW" = "A3" ]
  then 

    FILE_NAME=bacup_cfg/"$ip"/"$DATE"_"$ip".cfg
    if [ -d /srv/tftp/$ip/ ]
    then
    touch /srv/tftp/$FILE_NAME
    chmod 777 /srv/tftp/$FILE_NAME
    else
    mkdir /srv/tftp/bacup_cfg/$ip/ > /dev/null
    touch /srv/tftp/$FILE_NAME
    chmod 777 /srv/tftp/$FILE_NAME
    fi
    $SNMPSET -v2c -c $CommunityRW $ip 1.3.6.1.4.1.171.12.1.2.18.4.0 i 2 > /dev/null
    $SNMPSET -v2c -c $CommunityRW $ip 1.3.6.1.4.1.171.12.1.2.18.1.1.3.3 a $TFTP_SERVER_IP \
    1.3.6.1.4.1.171.12.1.2.18.1.1.5.3 s $FILE_NAME \
    1.3.6.1.4.1.171.12.1.2.18.1.1.7.3 s config.cfg \
    1.3.6.1.4.1.171.12.1.2.18.1.1.8.3 i 2 \
    1.3.6.1.4.1.171.12.1.2.18.1.1.12.3 i 3 > /dev/null
    sleep 3
  else
  echo "$ip HW XZ" 
  echo "$ip" >> erorr_log_$data.log
fi
done < /home/user/ip_nbnh.txt
