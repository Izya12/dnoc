#/bin/bash
arg1=10
cd /home/user/test/ISM
#ip=$1
while read ip
do
VLAN=$(snmpwalk -v2c -c public $ip 1.3.6.1.4.1.171.11.113.1.3.2.7.8.1.2 | awk '{ print $4 }' | sed -e "s/\"//g")
VERSION=$(snmpwalk -v2c -c public $ip iso.3.6.1.2.1.16.19.2.0 | awk '{ print $5 }' | sed -e "s/\"//g")
let "arg1 += 1"
COMD="config igmp_snooping multicast_vlan $VLAN replace_source_ip 192.168.199.$arg1"
#echo "$COMD"
echo "Коммутатор $ip - HW: $VERSION"
#/home/user/test/ISM/dlink_nyagan.sh $ip "$COMD"
done < /home/user/test/ISM/nyagan_db_3200.txt
