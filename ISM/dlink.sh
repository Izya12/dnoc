#!/usr/bin/expect -f
set IP [lindex $argv 0]
set COMD [lindex $argv 1]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "*ser*ame:"
# Посылаем имя пользователя и ждем запроса пароля.
send "robot\r"
expect "*ass*ord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "7kArzIITio\r"
expect "*#"
send "$COMD\r"
expect "*#"
send "logout\r"
expect eof
