#!/usr/bin/expect -f
set IP [lindex $argv 0]
set IPTV [lindex $argv 1]
set timeout -1
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "*ser*ame:"
# Посылаем имя пользователя и ждем запроса пароля.
send "Admin\r"
expect "*ass*ord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "fhrfynjc\r"
expect "*#"
send "config igmp_snooping data_driven_learning max_learned_entry 1\r"
expect "*#"
send "enable igmp_snooping multicast_vlan\r"
expect "*#"
send "config igmp_snooping vlan_name $IPTV fast_leave enable\r"
expect "*#"
send "config igmp_snooping data_driven_learning vlan_name $IPTV aged_out enable \r"
expect "*#"
send "config igmp_snooping querier vlan_name $IPTV query_interval 125 max_response_time 10 robustness_variable 2 last_member_query_interval 1 state disable version 2\r"
expect "*#"
send "save all\r"
expect "*#"
send "logout\r"
expect eof
