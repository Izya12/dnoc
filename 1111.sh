#!/usr/bin/expect -f
set MAC [lindex $argv 0]
set IP [lindex $argv 1]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
sleep 3
spawn telnet $IP
match_max 100000
expect "UserName:"
# Посылаем имя пользователя и ждем запроса пароля.
send "Admin\r"
expect "PassWord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "fhrfynjc\r"
expect "*#"
send "show fdb mac_address $MAC\r"
for  {} 1 {} {
       expect {
             -timeout 3
             "*More*" { send " "
                           set results $expect_out(buffer)
                           set file [open "./tmp/exp-result_passport.txt" a+];
                           puts $file $results
                           close $file }
             "*#" { set results $expect_out(buffer)
                       set file [open "./tmp/exp-result_passport.txt" a+];
                       puts $file $results
                       close $file
                       exit }        
              }
           }
expect "*#"
send "logout\r"
expect eof
