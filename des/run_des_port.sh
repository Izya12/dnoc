#/bin/bash
. "/etc/CFG_FULL.cfg"
cd $pwd/Security

while read ip
do

  HW=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $ip iso.3.6.1.2.1.16.19.3.0 | awk '{ print $4 }' | sed -e "s/\"//g")

  if [ "$HW" = "C1" ]
  then

    COMD1="enable port_security trap_log"

    PORT=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $ip iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')

    if [ "$PORT" = "10" ]
    then

      COMD2="config port_security ports 1-8 admin_state enable max_learning_addr 10 lock_address_mode deleteonreset"

      $pwd/Security/comd.sh "$ip" "$COMD1" "$COMD2"

    elif [ "$PORT" = "28" ]
    then 

      COMD2="config port_security ports 1-24 admin_state enable max_learning_addr 10 lock_address_mode deleteonreset"

      $pwd/Security/comd.sh "$ip" "$COMD1" "$COMD2"

    elif [ "$PORT" = "52" ]
    then 

      COMD2="config port_security ports 1-48 admin_state enable max_learning_addr 10 lock_address_mode deleteonreset"

      $pwd/Security/comd.sh "$ip" "$COMD1" "$COMD2"

  else
    echo "$ip PORT XZ" 
  fi

  elif [ "$HW" = "A1" -o "$HW" = "B1" ]
  then

    COMD1="enable port_security trap_log"

    PORT=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $ip iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')

    if [ "$PORT" = "10" ]
    then

      COMD2="config port_security ports 1-8 admin_state enable max_learning_addr 10 lock_address_mode DeleteOnReset"

      $pwd/Security/comd_A1_B1.sh "$ip" "$COMD1" "$COMD2"

    elif [ "$PORT" = "28" ]
    then 

      COMD2="config port_security ports 1-24 admin_state enable max_learning_addr 10 lock_address_mode DeleteOnReset"

      $pwd/Security/comd_A1_B1.sh "$ip" "$COMD1" "$COMD2"

    elif [ "$PORT" = "52" ]
    then 

      COMD2="config port_security ports 1-48 admin_state enable max_learning_addr 10 lock_address_mode DeleteOnReset"

      $pwd/Security/comd_A1_B1.sh "$ip" "$COMD1" "$COMD2"

  else
    echo "$ip PORT XZ" 
  fi

  else

  echo "$ip to $HW"

  fi

done < $pwd/db_ip_test.txt
