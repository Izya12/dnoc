#/bin/bash
cd /home/user/test/des
rm tmp/medium.tmp
IP=$1
while read port_num
do

des=$(snmpwalk -v2c -c metroro $IP 1.0.8802.1.1.2.1.4.1.1.9 | nawk '{ print substr($1,28) , ($4) }' | grep "\.$port_num\." | awk '{print $2}' | /bin/sed -e "s/\"//g")
des_1=$(snmpwalk -v2c -c metrorw $IP 1.0.8802.1.1.2.1.4.2.1.3 | nawk '{ print substr($1,38) }' | sort -n | grep "^$port_num\." | awk -F "." '{OFS="."}{print $5,$6,$7,$8}')

if [ $des ];
then
#snmpset -v2c -c metrorw $IP iso.3.6.1.2.1.31.1.1.1.18.$port_num s "$des ip $des_1"
echo " send \"config ports $port_num description $des ip $des_1\r\"" >> tmp/medium.tmp
else
#snmpset -v2c -c metrorw $IP iso.3.6.1.2.1.31.1.1.1.18.$port_num s "___"
echo " send \"config ports $port_num description ___\r\"" >> tmp/medium.tmp
fi

done < /home/user/test/des/port.lost
cat tmp/high.tmp > dlink_des.sh
cat tmp/medium.tmp >> dlink_des.sh
cat tmp/low.tmp >> dlink_des.sh
chmod +x dlink_des.sh

echo "********************"
echo
cat dlink_des.sh
echo
echo "********************"

#/home/user/test/des/dlink_des.sh $IP
