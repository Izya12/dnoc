#/bin/bash
cd /home/user/test/des

while read IP
do

agr_name=$(snmpwalk -v2c -c metroro $IP 1.0.8802.1.1.2.1.4.1.1.9 | awk '{print $4}')
des_1=$(snmpwalk -v2c -c metrorw $IP 1.0.8802.1.1.2.1.4.2.1.3 | nawk '{ print substr($1,38) }' | sort -n | awk -F "." '{OFS="."}{print $5,$6,$7,$8}')
agr_port=$(snmpwalk -v2c -c metroro $IP 1.0.8802.1.1.2.1.4.1.1.8 | awk '{print $7, $8}' | /bin/sed -e "s/\"//g")

echo "Коммутатор $IP подключен к агригация $agr_name ip $des_1 порт $agr_port"

done < /home/user/test/name_sn/db_ip.txt