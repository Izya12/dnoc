#!/usr/bin/expect -f
set IP [lindex $argv 0]
#set COMD [lindex $argv 1]
#set COMD_1 [lindex $argv 2]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "UserName:"
# Посылаем имя пользователя и ждем запроса пароля.
send "Admin\r"
expect "PassWord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "fhrfynjc\r"
expect "*#"
send "config traffic_segmentation 1-24 forward_list 25-28\r"
expect "*#"
send "config traffic_segmentation 25-28 forward_list 1-28\r"
expect "*#"
send "save all\r"
expect "*#"
send "logout\r"
expect eof
