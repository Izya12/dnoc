#!/usr/bin/expect -f
set IP [lindex $argv 0]
set PORT [lindex $argv 1]
set PPPOE [lindex $argv 2]
set IPTV [lindex $argv 3]
set USER [lindex $argv 4]
set PASSWORD [lindex $argv 5]
set timeout -1
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "*ser*ame:"
# Посылаем имя пользователя и ждем запроса пароля.
send "$USER\r"
expect "*ass*ord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "$PASSWORD\r"
expect "*#"
send "config vlan vlanid 666 delete $PORT\r"
expect "*#"
send "config vlan vlanid 750 delete $PORT\r"
expect "*#"
send "config vlan vlanid 902 delete $PORT\r"
expect "*#"
send "config vlan vlanid $PPPOE delete $PORT\r"
expect "*#"
send "config igmp_snooping multicast_vlan $IPTV delete member_port $PORT\r"
expect "*#"
send "config igmp_snooping multicast_vlan $IPTV delete tag_member_port $PORT\r"
expect "*#"
send "config vlan vlanid $PPPOE add untagged $PORT\r"
expect "*#"
send "save all\r"
expect "*#"
send "logout\r"
expect eof
