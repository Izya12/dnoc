#/bin/bash
ip=$1
port=$2
vlan=$3
iptv=$4
user=$(cat CFG_FULL.cfg | grep User | awk '{print $2}')
password=$(cat CFG_FULL.cfg | grep Password | awk '{print $2}')
pwd=$(cat CFG_FULL.cfg | grep PWD | awk '{print $2}')
$pwd/conf_metro/iptv.sh $ip $port $vlan $iptv $user $password >> /dev/null