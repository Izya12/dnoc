#/bin/bash
cd /home/user/test/conf_metro
echo
echo
echo
echo 'Введите услуги для подачи абоненту'
echo '1 full пакет'
echo '2 IP-TV'
echo '3 Интернет'
echo '4 VoIP'
echo
echo
read usluga

echo
echo
printf 'Введите IP адрес коммутатора: '
read IP
echo
printf 'Введите порт абонента: '
read PORT
echo
printf 'Введите Vlan авторизации: '
read PPPOE
echo
echo

case "$usluga" in

"1" )
/home/user/test/conf_metro/full_olimp.sh $IP $PORT $PPPOE # > /dev/null | echo "Идет настройка коммутатора"
;;

"2" )
/home/user/test/conf_metro/iptv_olimp.sh $IP $PORT $PPPOE # > /dev/null | echo "Идет настройка коммутатора"
;;

"3" )
/home/user/test/conf_metro/internet_olimp.sh $IP $PORT $PPPOE # > /dev/null | echo "Идет настройка коммутатора"
;;

"4" )
/home/user/test/conf_metro/voip_olimp.sh $IP $PORT $PPPOE # > /dev/null | echo "Идет настройка коммутатора"
;;

         * )
   # Выбор по-умолчанию.
   # "Пустой" ввод тоже обрабатывается здесь.
   echo
   echo "Нет данных."
  ;;

esac

echo

#exit 0
