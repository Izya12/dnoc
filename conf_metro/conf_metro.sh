#/bin/bash
. "/etc/CFG_FULL.cfg"
cd $pwd/conf_metro
echo
echo
echo
echo 'Введите услуги для подачи абоненту'
echo '1 full пакет'
echo '2 IP-TV'
echo '3 Интернет'
echo '4 VoIP'
echo
echo
read usluga

echo
echo
printf 'Введите IP адрес коммутатора: '
read IP
echo ""
echo ""
HW=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.16.19.3.0 | awk '{ print $4 }' | sed -e "s/\"//g")
if [ "$HW" = "A1" -o "$HW" = "B1" ]
then
ip_tv=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.11.113.1.3.2.7.8.1.2 | awk '{ print $4 }' | sed -e "s/\"//g")
ip_tv2=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.11.113.1.3.2.7.8.1.1 | awk '{ print $4 }' | sed -e "s/\"//g")
elif [ "$HW" = "C1" -o "$HW" = "A3" ]
then 
ip_tv=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.12.64.3.1.1.2 | awk '{print $4}' | sed -e "s/\"//g")
ip_tv2=$(/usr/local/bin/snmpwalk -v2c -c $CommunityRO $IP 1.3.6.1.4.1.171.12.64.3.1.1.1 | awk '{print $4}' | sed -e "s/\"//g")
fi
printf "Vlan IP-TV: $ip_tv2"
echo ""
echo ""
printf 'Введите порт абонента: '
read PORT
echo
printf 'Введите Vlan авторизации: '
read PPPOE
echo
echo

HW=$(/usr/local/bin/snmpget -v2c -c $CommunityRO $IP iso.3.6.1.2.1.16.19.3.0 | awk '{ print $4 }' | sed -e "s/\"//g")

if [ "$HW" = "A1" -o "$HW" = "B1" ]
then

case "$usluga" in

"1" )
$pwd/conf_metro/full.sh $IP $PORT $PPPOE $ip_tv $user $password # > /dev/null | echo "Идет настройка коммутатора"
echo "IP: $IP"
echo "Port: $PORT"
echo "Vlan PPPoE: $PPPOE"
echo "Vlan IP-TV: $ip_tv2"
;;

"2" )
$pwd/conf_metro/iptv.sh $IP $PORT $PPPOE $ip_tv $user $password # > /dev/null | echo "Идет настройка коммутатора"
echo "IP: $IP"
echo "Port: $PORT"
echo "Vlan PPPoE: $PPPOE"
echo "Vlan IP-TV: $ip_tv2"
;;

"3" )
$pwd/conf_metro/internet.sh $IP $PORT $PPPOE $ip_tv $user $password # > /dev/null | echo "Идет настройка коммутатора"
echo "IP: $IP"
echo "Port: $PORT"
echo "Vlan PPPoE: $PPPOE"
echo "Vlan IP-TV: $ip_tv2"
;;

"4" )
$pwd/conf_metro/voip.sh $IP $PORT $PPPOE $ip_tv $user $password # > /dev/null | echo "Идет настройка коммутатора"
echo "IP: $IP"
echo "Port: $PORT"
echo "Vlan PPPoE: $PPPOE"
echo "Vlan IP-TV: $ip_tv2"
;;

  * )
# Выбор по-умолчанию.
# "Пустой" ввод тоже обрабатывается здесь.
echo
echo "Нет данных."
;;

esac

elif [ "$HW" = "C1" -o "$HW" = "A3" ]
then 

case "$usluga" in

"1" )
$pwd/conf_metro/full_C1.sh $IP $PORT $PPPOE $ip_tv $user $password # > /dev/null | echo "Идет настройка коммутатора"
echo "IP: $IP"
echo "Port: $PORT"
echo "Vlan PPPoE: $PPPOE"
echo "Vlan IP-TV: $ip_tv2"
;;

"2" )
$pwd/conf_metro/iptv_C1.sh $IP $PORT $PPPOE $ip_tv $user $password # > /dev/null | echo "Идет настройка коммутатора"
echo "IP: $IP"
echo "Port: $PORT"
echo "Vlan PPPoE: $PPPOE"
echo "Vlan IP-TV: $ip_tv2"
;;

"3" )
$pwd/conf_metro/internet_C1.sh $IP $PORT $PPPOE $ip_tv $user $password # > /dev/null | echo "Идет настройка коммутатора"
echo "IP: $IP"
echo "Port: $PORT"
echo "Vlan PPPoE: $PPPOE"
echo "Vlan IP-TV: $ip_tv2"
;;

"4" )
$pwd/conf_metro/voip_C1.sh $IP $PORT $PPPOE $ip_tv $user $password # > /dev/null | echo "Идет настройка коммутатора"
echo "IP: $IP"
echo "Port: $PORT"
echo "Vlan PPPoE: $PPPOE"
echo "Vlan IP-TV: $ip_tv2"
;;

  * )
# Выбор по-умолчанию.
# "Пустой" ввод тоже обрабатывается здесь.
echo
echo "Нет данных."
;;

esac

else
echo error
fi

echo

#exit 0
