#!/usr/bin/perl -w

# вводим переменные
# хост, который будем опрашивать
$snmp_host = '192.168.212.50';
# сообщество
$snmp_community = 'metroro';
# начало МИБ`а
$snmp_part_MIB = '.1.3.6.1.2.1.2.2.1';
# максимальный номер интерфейса
$IF_max = 53;
# подрубаем модуль, отвечающий за SNMP
use Net::SNMP;

# достаём время
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime time;
$year = $year + 1900;
$mon = $mon + 1;
$unix_time = time;


# открываем сессию SNMP
($session,$error)=Net::SNMP->session(Hostname => $snmp_host,
Community => $snmp_community);
die "session error: $error" unless($session);
# запускаем цикл по перебору всех доступных сетевых интерфейсов устройства

for($i = 1; $i < $IF_max + 1; $i++){

#  строим MIB для трафика
$snmp_MIB_traff_in = $snmp_part_MIB . ".10." . $i;
$snmp_MIB_traff_out = $snmp_part_MIB . ".16." . $i;
$traff_in = $session->get_request("$snmp_MIB_traff_in");
$traff_out = $session->get_request("$snmp_MIB_traff_out");
die "request error: " . $session->error unless(defined $traff_in);
# результаты по траффику
$traff_in = $traff_in->{"$snmp_MIB_traff_in"};
$traff_out = $traff_out->{"$snmp_MIB_traff_out"};

#  строим MIB для скорости интерфейса
$snmp_MIB_speed = $snmp_part_MIB . ".5." . $i;
$IF_speed = $session->get_request("$snmp_MIB_speed");
die "request error: " . $session->error unless(defined $IF_speed);
# результаты - скорость интерфейса
$IF_speed = $IF_speed->{"$snmp_MIB_speed"};

#  строим MIB для состояния интерфейса
$snmp_MIB_status = $snmp_part_MIB . ".8." . $i;
$IF_status = $session->get_request("$snmp_MIB_status");
die "request error: " . $session->error unless(defined $IF_status);
# результаты - состояние интерфейса
$IF_status = $IF_status->{"$snmp_MIB_status"};

# Отваливаемся от SNMP
$session->close;

1;

