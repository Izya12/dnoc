#!/usr/bin/expect -f
set IP [lindex $argv 0]
set COMD [lindex $argv 1]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "*ser*ame:"
# Посылаем имя пользователя и ждем запроса пароля.
send "robot\r"
expect "*ass*ord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "7kArzIITio\r"
expect "*#"
send "sh util cpu\r"
expect "*Refresh"
send "q\r"
expect "*#"
send "sh util ports\r"
expect "*Refresh"
send "n\r"
expect "*Refresh"
send "q\r"
expect "*#"
send "sh igmp_snooping group\r"
expect "*#"
send "sh packet ports 1-52\r"
expect "*#"
send "sh error ports 1-52\r"
expect "*#"
send "sh fdb\r"
expect "*#"
send "sh arpentry\r"
expect "*#"
send "sh tech\r"
expect "*#"
send "save all\r"
expect "*#"
send "logout\r"
expect eof
