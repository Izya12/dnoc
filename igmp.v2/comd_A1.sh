#!/usr/bin/expect -f
set IP [lindex $argv 0]
set COMD1 [lindex $argv 1]
set COMD2 [lindex $argv 2]
set COMD3 [lindex $argv 3]
set COMD4 [lindex $argv 4]
set COMD5 [lindex $argv 5]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "*ser*ame:"
# Посылаем имя пользователя и ждем запроса пароля.
send "dikonnikov\r"
expect "*ass*ord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "Izya121788\r"
expect "*#"
send "enable igmp_snooping multicast_vlan\r"
expect "*#"
send "enable igmp_snooping\r"
expect "*#"
send "create mcast_filter_profile profile_id 1 profile_name full_pool_iptv\r"
expect "*#"
send "config mcast_filter_profile profile_id 1 add 224.1.1.0-224.1.1.255\r"
expect "*#"
send "config mcast_filter_profile profile_id 1 add 230.11.11.0-230.11.11.255\r"
expect "*#"
send "config mcast_filter_profile profile_id 1 add 224.20.0.52\r"
expect "*#"
send "config mcast_filter_profile profile_id 1 add 224.50.0.50-224.50.0.51\r"
expect "*#"
send "config mcast_filter_profile profile_id 1 add 225.10.10.16\r"
expect "*#"
send "config mcast_filter_profile profile_id 1 add 225.50.50.130-225.50.50.141\r"
expect "*#"
send "config mcast_filter_profile profile_id 1 add 229.99.99.140\r"
expect "*#"
send "config mcast_filter_profile profile_id 1 add 227.1.1.0-227.1.1.255\r"
expect "*#"
send "config mcast_filter_profile profile_id 1 add 233.49.170.0-233.49.170.255\r"
expect "*#"
send "$COMD1\r"
expect "*#"
send "$COMD2\r"
expect "*#"
send "$COMD3\r"
expect "*#"
send "$COMD4\r"
expect "*#"
send "config igmp_snooping querier all query_interval 125 max_response_time 10 robustness_variable 2 last_member_query_interval 1 state disable\r"
expect "*#"
#send "$COMD5\r"
#expect "*#"
send "save all\r"
expect "*#"
send "logout\r"
expect eof
