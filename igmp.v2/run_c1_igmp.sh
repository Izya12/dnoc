#/bin/bash
. "/etc/CFG_FULL.cfg"
cd $pwd/igmp.v2

#Коммутаторы доступа
while read ip
do

HW=$($VARSNMPGET -v2c -c $CommunityRO $ip iso.3.6.1.2.1.16.19.3.0 | awk '{ print $4 }' | sed -e "s/\"//g")

################################################################################################################################



if [ "$HW" = "A1" -o "$HW" = "B1" ]
    then
      MODEL=$($VARSNMPGET -v2c -c $CommunityRO $ip iso.3.6.1.2.1.1.1.0 | awk '{print $5}' | sed -e "s/\"//g")
      if [[ "$MODEL" = DES-3200* ]]
      then
	#------------------------------------------------------------------------------------------------------------------------------------
	NAMEIPTV=$($VARSNMPWALK -v2c -c $CommunityRO $ip 1.3.6.1.4.1.171.11.113.1.3.2.7.8.1.2 | awk '{print $4}' | sed -e "s/\"//g")
	COMD1="config limited_multicast_addr ports 1-28 add profile_id 1"
	COMD2="config max_mcast_group ports 1-24 max_group 56"
	COMD3="config igmp_snooping vlan_name $NAMEIPTV fast_leave enable"
	COMD4="config max_mcast_group ports 25-28 max_group 1024"
	COMD5="config multicast vlan_filtering_mode vlanid $IDIPTV filter_unregistered_groups"
	$pwd/igmp.v2/comd_A1.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD4" "$COMD5"
      elif [[ "$MODEL" = DES-1228* ]]
      then
	NAMEIPTV=$($VARSNMPWALK -v2c -c $CommunityRO $ip 1.3.6.1.4.1.171.11.116.2.2.7.8.1.2 | awk '{print $4}')
	COMD1="config limited_multicast_addr ports 1-28 add profile_id 1"
	COMD2="config max_mcast_group ports 1-24 max_group 56"
	COMD3="config igmp_snooping vlan_name $NAMEIPTV fast_leave enable"
	COMD4="config max_mcast_group ports 25-28 max_group 1024"
	COMD5="config multicast vlan_filtering_mode vlanid $IDIPTV filter_unregistered_groups"
	$pwd/igmp.v2/comd_A1.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD4" "$COMD5"
      fi
#echo ""
    #------------------------------------------------------------------------------------------------------------------------------------#
   elif [ "$HW" = "C1" -o "$HW" = "A3" ]
   then 
     #------------------------------------------------------------------------------------------------------------------------------------#
     NAMEIPTV=$($VARSNMPWALK -v2c -c $CommunityRO $ip 1.3.6.1.4.1.171.12.64.3.1.1.2 | awk '{print $4}' | sed -e "s/\"//g")
     IDIPTV=$($VARSNMPWALK -v2c -c $CommunityRO $ip 1.3.6.1.4.1.171.12.64.3.1.1.1 | awk '{print $4}' | sed -e "s/\"//g")
     PORT=$($VARSNMPWALK -v2c -c $CommunityRO $ip iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')
#
     if [ "$PORT" = "10" ]
     then
#
     COMD1="config limited_multicast_addr ports 1-10 add profile_id 1"
     COMD2="config max_mcast_group ports 1-8 max_group 56"
     COMD3="config limited_multicast_addr ports 1-10 access permit"
     COMD4="config limited_multicast_addr vlanid $IDIPTV add profile_id 1"
     COMD5="config igmp_snooping vlan_name $NAMEIPTV fast_leave enable"
     COMD6="config max_mcast_group ports 9-10 max_group 1024"
     COMD7="config limited_multicast_addr vlanid $IDIPTV access permit"
     COMD8="config multicast vlan_filtering_mode vlanid $IDIPTV filter_unregistered_groups"
     $pwd/igmp.v2/comd_C1.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD4" "$COMD5" "$COMD6" "$COMD7" "$COMD8" 

     elif [ "$PORT" = "28" ]
     then #
#
     COMD1="config limited_multicast_addr ports 1-28 add profile_id 1"
     COMD2="config max_mcast_group ports 1-24 max_group 56"
     COMD3="config limited_multicast_addr ports 1-28 access permit"
     COMD4="config limited_multicast_addr vlanid $IDIPTV add profile_id 1"
     COMD5="config igmp_snooping vlan_name $NAMEIPTV fast_leave enable"
     COMD6="config max_mcast_group ports 25-28 max_group 1024"
     COMD7="config limited_multicast_addr vlanid $IDIPTV access permit"
     COMD8="config multicast vlan_filtering_mode vlanid $IDIPTV filter_unregistered_groups"
     $pwd/igmp.v2/comd_C1.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD4" "$COMD5" "$COMD6" "$COMD7" "$COMD8" 
      
     elif [ "$PORT" = "52" ]
     then 

     COMD1="config limited_multicast_addr ports 1-52 add profile_id 1"
     COMD2="config max_mcast_group ports 1-48 max_group 56"
     COMD3="config limited_multicast_addr ports 1-52 access permit"
     COMD4="config limited_multicast_addr vlanid $IDIPTV add profile_id 1"
     COMD5="config igmp_snooping vlan_name $NAMEIPTV fast_leave enable"
     COMD6="config max_mcast_group ports 49-52 max_group 1024"
     COMD7="config limited_multicast_addr vlanid $IDIPTV access permit"
     COMD8="config multicast vlan_filtering_mode vlanid $IDIPTV filter_unregistered_groups"
     $pwd/igmp.v2/comd_C1.sh "$ip" "$COMD1" "$COMD2" "$COMD3" "$COMD4" "$COMD5" "$COMD6" "$COMD7" "$COMD8" 

     else
     echo "$ip PORT XZ" 
     fi
      #------------------------------------------------------------------------------------------------------------------------------------#
    else
      #------------------------------------------------------------------------------------------------------------------------------------#
      /bin/echo "$ip $HW" >> $pwd/name_sn/erorr_log_$DATE.log
      #------------------------------------------------------------------------------------------------------------------------------------#
fi

################################################################################################################################

#done < $pwd/db_ip_test.txt
done < $pwd/db_ip_ng.txt
