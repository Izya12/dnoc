#!/usr/bin/expect -f
log_file -a /var/log/vpn.out
set IP [lindex $argv 0]
set IP2 [lindex $argv 1]
set timeout -1 
set MAC [exec cat tmp/$IP.txt | egrep -n '[[:alnum:]]{2}.[[:alnum:]]{2}.[[:alnum:]]{2}.[[:alnum:]]{2}' | cut -d' ' -f14 ]
spawn telnet $IP2
match_max 100000  
expect "UserName:"
# Посылаем имя пользователя и ждем запроса пароля.
send "Admin\r"
expect "PassWord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "fhrfynjc\r"
expect "*#"
send "show fdb mac_address $MAC\r"
expect "*#"    
send "logout\r"
expect eof

