#!/usr/bin/expect -f
set IP [lindex $argv 0]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "UserName:"
# Посылаем имя пользователя и ждем запроса пароля.
send "Admin\r"
expect "PassWord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "fhrfynjc\r"
expect "*#"
send "enable command logging\r"
expect "*#"
send "save all\r"
expect "*#"
send "logout\r"
expect eof
