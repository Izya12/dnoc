#!/usr/bin/expect -f
set IP [lindex $argv 0]
set COMD0 [lindex $argv 1]
set USER [lindex $argv 2]
set PASSWORD [lindex $argv 3]
set COMD1 [lindex $argv 4]
set COMD2 [lindex $argv 5]
set COMD3 [lindex $argv 6]
set timeout -1
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "*ser*ame:"
# Посылаем имя пользователя и ждем запроса пароля.
send "$USER\r"
expect "*ass*ord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "$PASSWORD\r"
expect "*#"
send "disable lldp\r"
expect "*#"
send "enable lldp\r"
expect "*#"
send "$COMD0\r"
expect "*#"
send "$COMD1\r"
expect "*#"
send "$COMD2\r"
expect "*#"
send "$COMD3\r"
expect "*#"
send "save all\r"
expect "*#"
send "logout\r"
expect eof
