#!/usr/bin/expect -f
set IP [lindex $argv 0]
set COMD0 [lindex $argv 1]
set timeout -1 
spawn ping -c 2 -i 3 -W 1 $IP
spawn telnet $IP
match_max 100000
expect "*ser*ame:"
# Посылаем имя пользователя и ждем запроса пароля.
send "dikonnikov\r"
expect "*ass*ord:"
# Посылаем пароль и ждем приглашения ввода командного интерпретатора.
send "4VDSWAchlh\r"
expect "*#"
send "enable admin\r"
expect "*ord:"
send "tGswEbYtPD\r"
expect "*#"
send "disable lldp\r"
expect "*#"
send "enable lldp\r"
expect "*#"
send "config lldp ports 1-27 admin_status tx_and_rx\r"
expect "*#"
send "config lldp ports 1-27 dot1_tlv_vlan_name vlanid 1-4094 enable \r"
expect "*#"
send "config lldp ports 1-27 dot1_tlv_protocol_identity lacp enable                   \r"
expect "*#"
send "config lldp ports 1-27 dot1_tlv_protocol_identity gvrp enable \r"
expect "*#"
send "config lldp ports 1-27 dot1_tlv_protocol_identity stp enable \r"
expect "*#"
send "config lldp ports 1-27 notification enable\r"
expect "*#"
send "config lldp ports 1-27 basic_tlvs port_description system_name system_description system_capabilities enable\r"
expect "*#"
send "config lldp ports 1-27 dot1_tlv_pvid enable \r"
expect "*#"
send "config lldp ports 1-27 dot1_tlv_protocol_identity eapol enable \r"
expect "*#"
send "config lldp ports 1-27 dot3_tlvs mac_phy_configuration_status link_aggregation  maximum_frame_size enable\r"
expect "*#"
send "$COMD0\r"
expect "*#"
send "save all\r"
expect "*#"
send "logout\r"
expect eof