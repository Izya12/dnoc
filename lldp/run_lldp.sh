#/bin/bash
. "/etc/CFG_FULL.cfg"
cd $pwd/lldp

nmap --excludefile $pwd/db_ip_exclude.txt -sn 192.168.212.0/22 | egrep -v "Host" | awk '{print $5}' > $pwd/db_ip_test.txt
nmap --excludefile $pwd/db_ip_agr_exclude.txt -sn 192.168.212.2-49 | egrep -v "Host" | awk '{print $5}' > $pwd/db_ip_agr.txt

# Коммутаторы агрегации

while read ip
do

COMD0="config lldp ports 1-27 mgt_addr ipv4 $ip enable"
$pwd/lldp/dlink_agr.sh $ip "$COMD0"

done < $pwd/db_ip_agr.txt

# Коммутаторы доступа
while read ip
do

PORT=$($SNMPWALK -v2c -c $CommunityRO $ip iso.3.6.1.2.1.17.1.2.0 | awk '{print $4}')

if [ "$PORT" = "10" ]
then

COMD0="config lldp ports 9-10 mgt_addr ipv4 $ip enable"
COMD1="config lldp ports 9-10 notification enable"
COMD2="config lldp ports 9-10 admin_status tx_and_rx"
COMD3="config lldp ports 9-10 basic_tlvs port_description system_name system_description system_capabilities enable"
$pwd/lldp/dlink.sh $ip "$COMD0" "$user" "$password" "$COMD1" "$COMD2" "$COMD3"

elif [ "$PORT" = "28" ]
then 

COMD0="config lldp ports 25-28 mgt_addr ipv4 $ip enable"
COMD1="config lldp ports 25-28 notification enable"
COMD2="config lldp ports 25-28 admin_status tx_and_rx"
COMD3="config lldp ports 25-28 basic_tlvs port_description system_name system_description system_capabilities enable"
$pwd/lldp/dlink.sh $ip "$COMD0" "$user" "$password" "$COMD1" "$COMD2" "$COMD3"

elif [ "$PORT" = "52" ]
then 

COMD0="config lldp ports 49-52 mgt_addr ipv4 $ip enable"
COMD1="config lldp ports 49-52 notification enable"
COMD2="config lldp ports 49-52 admin_status tx_and_rx"
COMD3="config lldp ports 49-52 basic_tlvs port_description system_name system_description system_capabilities enable"
$pwd/lldp/dlink.sh $ip "$COMD0" "$user" "$password" "$COMD1" "$COMD2" "$COMD3"

else
echo "$ip PORT XZ" 
echo "$ip" >> erorr_log.log
fi

done < $pwd/db_ip_test.txt